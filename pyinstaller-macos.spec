# -*- mode: python -*-

block_cipher = None


a = Analysis(['opendesktopsharing/src/main.py'],
             pathex=['opendesktopsharing/src', 'opendesktopsharing/src/lib', 'opendesktopsharing/share', ],
             binaries=[
             ],
             datas=[
                ('opendesktopsharing/share/images/get_info-mini.png', './share/images', ),
                ('opendesktopsharing/share/images/exit-mini.png', './share/images', ),
                ('opendesktopsharing/share/images/preferences_system-mini.png', './share/images/', ),
                ('opendesktopsharing/share/images/logo-medium.png', './share/images', ),
                ('opendesktopsharing/share/images/odc.ico', './share/images',),
                ('opendesktopsharing/share/config/settings.ini', './share/config',),
                ('opendesktopsharing/share/locale', './share/locale'),
                #('README.md', './share/'),
                ('opendesktopsharing/share/license/gpl-3.0.txt', './share/license'),
             ],
             hiddenimports=[
             ],
             hookspath=[],
             runtime_hooks=[],
             excludes=[
             ],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='OpenDesktopSharing',
          debug=False,
          strip=True,
          upx=False,
          console=False, )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=True,
               upx=False,
               console=False,
               name='OpenDesktopSharing')
app = BUNDLE(coll,
             console=False,
             name='OpenDesktopSharing.app',
             icon='./opendesktopsharing/share/images/odc.icns',
             bundle_identifier=None)