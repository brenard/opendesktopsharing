# Translations template for opendesktopsharing.
# Copyright (C) 2022 ORGANIZATION
# This file is distributed under the same license as the opendesktopsharing
# project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2022.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: opendesktopsharing 1.1\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2022-02-28 20:07+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.6.0\n"

#: opendesktopsharing/src/lib/guest.py:51
#: opendesktopsharing/src/lib/show_capture.py:48
msgid "test"
msgstr ""

#: opendesktopsharing/src/lib/main_window.py:135
msgid "&File"
msgstr ""

#: opendesktopsharing/src/lib/main_window.py:137
msgid "&Help"
msgstr ""

#: opendesktopsharing/src/lib/main_window.py:139
msgid "&Settings\tCtrl+P"
msgstr ""

#: opendesktopsharing/src/lib/main_window.py:139
msgid "Settings dialog"
msgstr ""

#: opendesktopsharing/src/lib/main_window.py:145
msgid "&Quit\tCtrl+Q"
msgstr ""

#: opendesktopsharing/src/lib/main_window.py:145
msgid "Quit application"
msgstr ""

#: opendesktopsharing/src/lib/main_window.py:151
msgid "&About"
msgstr ""

#: opendesktopsharing/src/lib/main_window.py:151
msgid "About application"
msgstr ""

#: opendesktopsharing/src/lib/main_window.py:197
msgid "Allow remote connexion"
msgstr ""

#: opendesktopsharing/src/lib/main_window.py:198
msgid "Your ID :"
msgstr ""

#: opendesktopsharing/src/lib/main_window.py:201
msgid "Your password :"
msgstr ""

#: opendesktopsharing/src/lib/main_window.py:204
#: opendesktopsharing/src/lib/main_window.py:234
msgid "Connect"
msgstr ""

#: opendesktopsharing/src/lib/main_window.py:205
#: opendesktopsharing/src/lib/main_window.py:235
msgid "Disconnect"
msgstr ""

#: opendesktopsharing/src/lib/main_window.py:227
msgid "Connect on remote desktop"
msgstr ""

#: opendesktopsharing/src/lib/main_window.py:228
msgid "Type the remote ID :"
msgstr ""

#: opendesktopsharing/src/lib/main_window.py:231
msgid "Type the password :"
msgstr ""

#: opendesktopsharing/src/lib/main_window.py:327
msgid "Check for update"
msgstr ""

#: opendesktopsharing/src/lib/main_window.py:344
#: opendesktopsharing/src/lib/main_window.py:345
msgid "Unable to join the server"
msgstr ""

#: opendesktopsharing/src/lib/main_window.py:351
msgid "Wrong version (Versions accepted: {} != your client version: {})"
msgstr ""

#: opendesktopsharing/src/lib/main_window.py:363
msgid "Up to date ({})"
msgstr ""

#: opendesktopsharing/src/lib/main_window.py:367
msgid ""
"\n"
"        Config file is corrupted or obsolete version found,\n"
"        Please, fill settings window or close the app.\n"
"        "
msgstr ""

#: opendesktopsharing/src/lib/main_window.py:410
#: opendesktopsharing/src/lib/main_window.py:419
msgid "Copied {} into clipboard"
msgstr ""

#: opendesktopsharing/src/lib/main_window.py:424
msgid "Your system is unsupported"
msgstr ""

#: opendesktopsharing/src/lib/main_window.py:430
msgid "Hosting session to main server ... ({})"
msgstr ""

#: opendesktopsharing/src/lib/main_window.py:444
msgid "Session created ..."
msgstr ""

#: opendesktopsharing/src/lib/main_window.py:445
msgid "Starting remote access server:"
msgstr ""

#: opendesktopsharing/src/lib/main_window.py:447
msgid "Waiting for connection ..."
msgstr ""

#: opendesktopsharing/src/lib/main_window.py:488
msgid "Please, type ID and password"
msgstr ""

#: opendesktopsharing/src/lib/main_window.py:587
msgid "Remote user is helping you..."
msgstr ""

#: opendesktopsharing/src/lib/main_window.py:600
msgid "Connected to the host"
msgstr ""

#: opendesktopsharing/src/lib/settings_window.py:53
msgid "OK"
msgstr ""

#: opendesktopsharing/src/lib/settings_window.py:54
msgid "Cancel"
msgstr ""

#: opendesktopsharing/src/lib/settings_window.py:55
msgid "Load defaults values"
msgstr ""

#: opendesktopsharing/src/lib/settings_window.py:62
msgid "Connection settings"
msgstr ""

#: opendesktopsharing/src/lib/settings_window.py:66
msgid "Server: (ws://servername:port/path)"
msgstr ""

#: opendesktopsharing/src/lib/settings_window.py:73
msgid "Proxy settings:"
msgstr ""

#: opendesktopsharing/src/lib/settings_window.py:74
msgid "Use proxy"
msgstr ""

#: opendesktopsharing/src/lib/settings_window.py:79
msgid "Host:"
msgstr ""

#: opendesktopsharing/src/lib/settings_window.py:86
msgid "Port:"
msgstr ""

#: opendesktopsharing/src/lib/settings_window.py:96
msgid "Quality settings (Host side)"
msgstr ""

#: opendesktopsharing/src/lib/settings_window.py:109
msgid "Colors depth:"
msgstr ""

#: opendesktopsharing/src/lib/show_capture.py:98
msgid "OpenDesktopSharing: Loading..."
msgstr ""

