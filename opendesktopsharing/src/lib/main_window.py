#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
OpenDesktopSharing



The OpenDesktopSharing's goal is to access to remote computer across firewall and proxy:
- No needed to know IPs address
- Access only need KEY/PASSWORD
- Use websocket VPN to connect server and client
- Use Auth server (OpenDesktopSharing-Server) to generate auth access

@author: Pierre Arnaud <parnaud@easter-eggs.com>
@copyright: 2022

OpenDesktopSharing is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenDesktopSharing is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OpenDesktopSharing.  If not, see <http://www.gnu.org/licenses/>.
"""

import appdirs
import configparser
import gettext
import locale
import logging
import logging.config
import logging.handlers
import os
import platform
import sys
import time
from threading import Thread
from websocket import create_connection
from websocket import ABNF
import wx
import wx.adv
import wx.lib.inspection
from wx.lib.wordwrap import wordwrap

from opendesktopsharing import __version__
from opendesktopsharing import PROTOCOLE_VERSION
from opendesktopsharing import APP_NAME
from opendesktopsharing import COPYRIGHT
from opendesktopsharing import WEBSITE
from opendesktopsharing import DEVELOPER
from opendesktopsharing import COMPANY
from opendesktopsharing import INFO_LICENCE
from opendesktopsharing import NEW_VERSION_ALERT
from opendesktopsharing import ODS_DESCRIPTION_TXT
from opendesktopsharing.src.lib import commons
from opendesktopsharing.src.lib.host import HostWebSocket
from opendesktopsharing.src.lib.guest import GuestWebSocket
from opendesktopsharing.src.lib.settings import Settings
from opendesktopsharing.src.lib.settings_window import SettingsWindow
from opendesktopsharing.src.lib.show_capture import ShowCapture


log = logging.getLogger('OpenDesktopSharing')


class CustomConsoleHandler(logging.StreamHandler):
    """"""

    # ----------------------------------------------------------------------
    def __init__(self, textctrl):
        """"""

        logging.StreamHandler.__init__(self)
        formatter = logging.Formatter('%(message)s')
        self.setFormatter(formatter)
        self.textctrl = textctrl

    # ----------------------------------------------------------------------
    def emit(self, record):
        """Constructor"""
        msg = self.format(record)
        try:
            wx.CallAfter(self.textctrl.AppendText, msg + "\n")
        except Exception as e:
            pass
        self.flush()


class ConnectionWindow(wx.Frame):

    def __init__(self, parent, id, title, settings):
        wx.Frame.__init__(self, parent, wx.ID_ANY, title,
                          style=wx.DEFAULT_FRAME_STYLE & ~ (wx.RESIZE_BORDER | wx.MAXIMIZE_BOX))

        language_file = commons.get_locale_path()
        log.info(language_file)
        try:
            if 'fr_FR' in locale.getdefaultlocale():
                self.presLan_fr = gettext.translation('opendesktopsharing', language_file, languages=['fr']) # French
                self.presLan_fr.install()
            else:
                gettext.install('opendesktopsharing', language_file)
        except Exception as e:
            log.warning(e)
            gettext.install('opendesktopsharing', language_file)

        # INIT VAR
        self.settings = settings

        self.remote_status = None

        self.host_websocket = None
        self.guest_websocket = None

        # Check for valid config file
        if self.settings.force_restart is not None:
            self.force_restart()

        # FONTS
        title_font = wx.Font(10, wx.SWISS, wx.NORMAL, wx.NORMAL)
        ids_font = wx.Font(11, wx.SWISS, wx.NORMAL, wx.LIGHT)
        label_font = wx.Font(11, wx.DEFAULT, wx.NORMAL, wx.NORMAL)


        # MENU BAR
        self.menu_bar = wx.MenuBar()

        self.file_menu = wx.Menu()
        self.menu_bar.Append(self.file_menu, _('&File'))
        self.about_menu = wx.Menu()
        self.menu_bar.Append(self.about_menu, _('&Help'))

        self.pref_menu_item = wx.MenuItem(self.file_menu, wx.ID_PREFERENCES, _("&Settings\tCtrl+P"), _("Settings dialog"))
        pref_menu_icon = commons.get_data_file_path('images', 'preferences_system-mini.png')
        self.pref_menu_item.SetBitmap(wx.Image(pref_menu_icon, wx.BITMAP_TYPE_PNG).ConvertToBitmap())
        self.file_menu.Append(self.pref_menu_item)
        self.Bind(wx.EVT_MENU, self.open_settings, id=wx.ID_PREFERENCES)

        self.quit_menu_item = wx.MenuItem(self.file_menu, wx.ID_EXIT, _('&Quit\tCtrl+Q'), _('Quit application'))
        quit_menu_icon = commons.get_data_file_path('images', 'exit-mini.png')
        self.quit_menu_item.SetBitmap(wx.Image(quit_menu_icon, wx.BITMAP_TYPE_PNG).ConvertToBitmap())
        self.file_menu.Append(self.quit_menu_item)
        self.Bind(wx.EVT_MENU, self.quit_action, self.quit_menu_item)

        self.about_menu_item = wx.MenuItem(self.file_menu, wx.ID_ABOUT, _('&About'), _('About application'))
        about_menu_icon = commons.get_data_file_path('images', 'get_info-mini.png')
        self.about_menu_item.SetBitmap(wx.Image(about_menu_icon, wx.BITMAP_TYPE_PNG).ConvertToBitmap())
        self.about_menu.Append(self.about_menu_item)
        self.Bind(wx.EVT_MENU, self.open_about, self.about_menu_item)

        self.SetMenuBar(self.menu_bar)

        # ICON
        icon = commons.get_data_file_path('images', 'odc.ico')
        self.SetIcon(wx.Icon(icon, wx.BITMAP_TYPE_ICO))

        # HEADER
        bitmap = wx.Bitmap(commons.get_data_file_path('images', 'logo-medium.png'))

        # MAIN PANEL
        self.panel = wx.Panel(self)
        # Status BAR
        self.status_bar = self.CreateStatusBar(2, style=wx.STB_SHOW_TIPS)

        self.header_panel = wx.Panel(self.panel, wx.ID_ANY)

        self.host_panel = wx.Panel(self.panel, wx.ID_ANY)
        self.host_panel_fields = wx.Panel(self.host_panel, wx.ID_ANY)
        self.host_panel.SetBackgroundColour('white')

        self.guest_panel = wx.Panel(self.panel, wx.ID_ANY)
        self.guest_panel_fields = wx.Panel(self.guest_panel, wx.ID_ANY)
        self.guest_panel.SetBackgroundColour('white')

        # BOXES
        self.main_box = wx.BoxSizer(wx.VERTICAL)
        self.header_box = wx.BoxSizer(wx.HORIZONTAL)
        self.host_guest_box = wx.BoxSizer(wx.HORIZONTAL)
        self.host_box = wx.BoxSizer(wx.VERTICAL)
        self.host_box_fields = wx.BoxSizer(wx.VERTICAL)
        self.host_box_buttons = wx.BoxSizer(wx.HORIZONTAL)
        self.guest_box = wx.BoxSizer(wx.VERTICAL)
        self.guest_box_fields = wx.BoxSizer(wx.VERTICAL)
        self.guest_box_buttons = wx.BoxSizer(wx.HORIZONTAL)

        # HEADER
        self.header_logo = wx.StaticBitmap(self.header_panel, size=(488, -1))
        self.header_logo.SetBitmap(bitmap)
        self.header_box.Add(self.header_logo, 0, wx.ALL|wx.ALIGN_CENTER, border=-1)
        # HOST
        self.host_support_title = wx.StaticText(self.host_panel, label=_('Allow remote connexion'))
        self.host_support_id_label = wx.StaticText(self.host_panel_fields, label=_('Your ID :'))
        self.host_support_id_field = wx.TextCtrl(self.host_panel_fields, style=wx.TE_CENTRE|wx.TE_READONLY)

        self.host_support_pwd_label = wx.StaticText(self.host_panel_fields, label=_('Your password :'))
        self.host_support_pwd_field = wx.TextCtrl(self.host_panel_fields, style=wx.TE_CENTRE|wx.TE_READONLY)

        self.host_support_connect_btn = wx.Button(self.host_panel, label=_('Connect'), size=(100, 30))
        self.host_support_disconnect_btn = wx.Button(self.host_panel, label=_('Disconnect'), size=(100, 30))

        self.host_box_fields.Add((200, 20))
        self.host_box_fields.Add(self.host_support_id_label, 0, wx.ALIGN_CENTER_HORIZONTAL, 10)
        self.host_box_fields.Add(self.host_support_id_field, 0, wx.ALIGN_CENTER_HORIZONTAL, 10)
        self.host_box_fields.Add((200, 10))
        self.host_box_fields.Add(self.host_support_pwd_label, 0, wx.ALIGN_CENTER_HORIZONTAL, 10)
        self.host_box_fields.Add(self.host_support_pwd_field, 0, wx.ALIGN_CENTER_HORIZONTAL, 10)
        self.host_box_fields.Add((200, 20))
        self.host_box_buttons.Add(self.host_support_connect_btn, 0, wx.ALL, 10)
        self.host_box_buttons.Add(self.host_support_disconnect_btn, 0, wx.ALL, 10)

        self.host_box.AddStretchSpacer(prop=1)
        self.host_box.Add((10, 10))
        self.host_box.Add(self.host_support_title, 0, wx.ALIGN_CENTER, 10)
        self.host_box.Add((20, 20))
        self.host_box.Add(self.host_panel_fields, 0, wx.ALIGN_CENTER, 10)
        self.host_box.Add((20, 20))
        self.host_box.Add(self.host_box_buttons, 0, wx.ALIGN_CENTER, 10)
        self.host_box.AddStretchSpacer(prop=1)

        # GUEST
        self.guest_support_title = wx.StaticText(self.guest_panel, label=_('Connect on remote desktop'))
        self.guest_support_id_label = wx.StaticText(self.guest_panel_fields, label=_('Type the remote ID :'))
        self.guest_support_id_field = wx.TextCtrl(self.guest_panel_fields, style=wx.TE_CENTRE)

        self.guest_support_pwd_label = wx.StaticText(self.guest_panel_fields, label=_('Type the password :'))
        self.guest_support_pwd_field = wx.TextCtrl(self.guest_panel_fields, style=wx.TE_CENTRE|wx.TE_PASSWORD)

        self.guest_support_connect_btn = wx.Button(self.guest_panel, label=_('Connect'), size=(100, 30))
        self.guest_support_disconnect_btn = wx.Button(self.guest_panel, label=_('Disconnect'), size=(100, 30))

        self.guest_box_fields.Add((200, 20))
        self.guest_box_fields.Add(self.guest_support_id_label, 0, wx.ALIGN_CENTER_HORIZONTAL, 10)
        self.guest_box_fields.Add(self.guest_support_id_field, 0, wx.ALIGN_CENTER_HORIZONTAL, 10)
        self.guest_box_fields.Add((200, 10))
        self.guest_box_fields.Add(self.guest_support_pwd_label, 0, wx.ALIGN_CENTER_HORIZONTAL, 10)
        self.guest_box_fields.Add(self.guest_support_pwd_field, 0, wx.ALIGN_CENTER_HORIZONTAL, 10)
        self.guest_box_fields.Add((200, 20))
        self.guest_box_buttons.Add(self.guest_support_connect_btn, 0, wx.ALL, 10)
        self.guest_box_buttons.Add(self.guest_support_disconnect_btn, 0, wx.ALL, 10)

        self.guest_box.AddStretchSpacer(prop=1)
        self.guest_box.Add((10, 10))
        self.guest_box.Add(self.guest_support_title, 0, wx.ALIGN_CENTER, 10)
        self.guest_box.Add((20, 20))
        self.guest_box.Add(self.guest_panel_fields, 0, wx.ALIGN_CENTER, 10)
        self.guest_box.Add((20, 20))
        self.guest_box.Add(self.guest_box_buttons, 0, wx.ALIGN_CENTER, 10)
        self.guest_box.AddStretchSpacer(prop=1)

        # SET FONTS
        self.host_support_title.SetFont(title_font)
        self.host_support_id_label.SetFont(label_font)
        self.host_support_id_field.SetFont(ids_font)
        self.host_support_pwd_label.SetFont(label_font)
        self.host_support_pwd_field.SetFont(ids_font)
        self.guest_support_title.SetFont(title_font)
        self.guest_support_id_label.SetFont(label_font)
        self.guest_support_id_field.SetFont(ids_font)
        self.guest_support_pwd_label.SetFont(label_font)

        # BIND
        self.host_support_id_field.Bind(wx.EVT_LEFT_DOWN, self.host_support_id_field_copy)
        self.host_support_pwd_field.Bind(wx.EVT_LEFT_DOWN, self.host_support_pwd_field_copy)
        self.host_support_connect_btn.Bind(wx.EVT_BUTTON, self.host_connect_action)
        self.host_support_disconnect_btn.Bind(wx.EVT_BUTTON, self.host_disconnect_action)
        self.guest_support_connect_btn.Bind(wx.EVT_BUTTON, self.guest_connect_action)
        self.guest_support_disconnect_btn.Bind(wx.EVT_BUTTON, self.guest_disconnect_action)
        self.Bind(wx.EVT_CLOSE, self.quit_action)

        # Set start state
        self.set_disable_host_connect_disconnect_buttons()
        self.set_host_connect_button()
        self.set_disable_guest_connect_disconnect_buttons()
        self.set_guest_connect_button()

        self.SetTitle('OpenDesktopSharing')

        self.main_box.Add(self.header_panel, 0, wx.ALIGN_CENTER, border=0)

        self.host_guest_box.AddStretchSpacer(prop=1)
        self.guest_box.Add((0, 0))
        self.host_guest_box.Add(self.host_panel, 0, wx.ALIGN_CENTER , border=0)

        self.host_guest_box.AddSpacer(size=4)
        self.host_guest_box.AddStretchSpacer(prop=1)

        self.host_guest_box.Add(self.guest_panel, 0, wx.ALIGN_CENTER, border=0)
        self.guest_box.Add((0, 0))
        self.host_guest_box.AddStretchSpacer(prop=1)

        self.main_box.Add(self.host_guest_box, 0, wx.ALIGN_CENTER, border=0)

        # INIT
        self.host_websocket = None
        self.host_watcher = None
        self.guest_watcher = None

        self.screen_width, self.screen_height = wx.DisplaySize()

        self.header_panel.SetSizer(self.header_box)
        self.host_panel.SetSizer(self.host_box)
        self.guest_panel.SetSizer(self.guest_box)
        self.host_panel_fields.SetSizer(self.host_box_fields)
        self.guest_panel_fields.SetSizer(self.guest_box_fields)
        self.show_capture = None

        self.Show(True)
        self.Refresh()
        self.guest_support_id_field.SetFocus()

        # wx.lib.inspection.InspectionTool().Show()
        if self.check_update():
            self.host_connect_action(None)
        else:
            self.set_disable_host_connect_disconnect_buttons()
            self.set_disable_guest_connect_disconnect_buttons()

        wx.CallAfter(self.resize)

    def check_update(self):
        log.info(_('Check for update'))

        download_url = ""
        accepted_client_versions = []
        try:
            ws = create_connection(self.settings.client['auth'], timeout=2, sslopt=self.settings.sslopt)
            ws.send(b'SERVICE - CHECK VERSION ' + bytes(str(PROTOCOLE_VERSION), 'utf-8'), opcode=ABNF.OPCODE_BINARY)
            response =  ws.recv()
            if b'VERSION NOK - ' in response:
                data = response.split(b' - ')[-1]
                download_url, accepted_client_versions = data.split(b'__')
                download_url = download_url.split(b'=_=')[-1].decode('utf-8')
                accepted_client_versions = [float(el) for el in accepted_client_versions.split(b'=_=')[-1].decode('utf-8').split(',')]
            else:
                return True
        except Exception as e:
            log.error(e)
            self.set_status_host(_('Unable to join the server'))
            log.warning(_('Unable to join the server'))
            return None

        if PROTOCOLE_VERSION not in accepted_client_versions:
            log.info(_('Wrong version (Versions accepted: {} != your client version: {})'.format(
                str(', '.join([str(el) for el in accepted_client_versions])),
                str(PROTOCOLE_VERSION),

            )))
            info = wx.adv.AboutDialogInfo()
            info.SetDescription(wordwrap(_(NEW_VERSION_ALERT.format(
                PROTOCOLE_VERSION,
                str(', '.join([str(el) for el in accepted_client_versions])),
                download_url)
            ), 350, wx.ClientDC(self.panel)))
            wx.adv.AboutBox(info)
            return False

        log.info(_('Up to date ({})'.format(str(PROTOCOLE_VERSION))))
        return True

    def force_restart(self):
        message = _("""
        Config file is corrupted or obsolete version found,
        Please, fill settings window or close the app.
        """)
        wx.MessageBox(message, 'Infos', wx.OK | wx.ICON_INFORMATION)
        self.open_settings(None, exit_on_cancel=True)

    def open_about(self, event):
        info = wx.adv.AboutDialogInfo()
        info.SetName(APP_NAME)
        info.SetVersion(str(__version__))
        info.SetCopyright(COPYRIGHT)
        info.SetDescription(wordwrap(_(ODS_DESCRIPTION_TXT), 350, wx.ClientDC(self.panel)))
        info.SetWebSite(WEBSITE)
        info.SetDevelopers([DEVELOPER, COMPANY])
        info_license = INFO_LICENCE
        info.SetLicense(wordwrap(info_license, 500, wx.ClientDC(self.panel)))
        wx.adv.AboutBox(info)

    def open_settings(self, event, exit_on_cancel=None):
        settings_dialog = SettingsWindow(self.settings, self)
        res = settings_dialog.ShowModal()
        if exit_on_cancel and res == wx.ID_CANCEL:
            self.quit_action(None)
        elif res == wx.ID_OK:
            self.settings.save()
            if self.host_websocket:
                self.host_websocket.settings = self.settings
            log.info('Config saved to {}'.format(self.settings.config_uri))

        settings_dialog.Destroy()

    def file_menu_action(self, event):
        event_id = event.GetId()
        if event_id == wx.ID_EXIT:
            self.quit_action(event)

    def host_support_id_field_copy(self, event):
        clipdata = wx.TextDataObject()
        clipdata.SetText(self.host_support_id_field.GetLabelText())
        wx.TheClipboard.Open()
        wx.TheClipboard.SetData(clipdata)
        wx.TheClipboard.Close()
        self.set_status_main(_("Copied {} into clipboard".format(self.host_support_id_field.GetLabelText())))
        event.Skip()

    def host_support_pwd_field_copy(self, event):
        clipdata = wx.TextDataObject()
        clipdata.SetText(self.host_support_pwd_field.GetLabelText())
        wx.TheClipboard.Open()
        wx.TheClipboard.SetData(clipdata)
        wx.TheClipboard.Close()
        self.set_status_main(_("Copied {} into clipboard".format(self.host_support_pwd_field.GetLabelText())))
        event.Skip()

    def host_connect_action(self, event):
        if platform.system() == "Linux" and os.environ.get("XDG_SESSION_TYPE") == "wayland":
            self.host_support_connect_btn.SetLabelText(_('Your system is unsupported'))
            self.host_support_disconnect_btn.Hide()
            self.host_box.Hide(True)
            self.host_support_connect_btn.Enable(False)
            return None
        self.set_disable_host_connect_disconnect_buttons()
        self.set_status_host(_('Hosting session to main server ... ({})').format(self.settings.client['auth']))

        self.host_websocket = HostWebSocket(
            self.settings,
        )
        self.host_websocket.daemon = True
        self.host_websocket.start()
        while self.host_websocket.channel is None:
            if self.host_websocket.error:
                wx.MessageBox(str(self.host_websocket.error), 'Error', wx.OK | wx.ICON_ERROR)
                self.set_status_host(str(self.host_websocket.error))
                self.host_disconnect_action(None)
                return None
            time.sleep(.2)
        self.set_status_host(_('Session created ...'))
        self.set_status_host(_('Starting remote access server:'))

        self.set_status_host(_('Waiting for connection ...'))
        self.host_watcher = Thread(target=self.watch_process_host)
        self.host_watcher.start()
        if self.host_websocket:
            self.set_host_key_field(self.host_websocket.id)
            self.set_host_password_field(self.host_websocket.pwd)
        self.set_host_disconnect_button()
        self.resize()

    def host_disconnect_action(self, event):
        self.kill_host_process()
        self.set_host_connect_button()
        self.host_empty_fields()
        self.resize()

    def guest_connect_action(self, event):
        self.show_capture = ShowCapture()
        if self.get_guest_password_field() and self.get_guest_key_field():
            self.set_disable_guest_connect_disconnect_buttons()

            self.guest_websocket = GuestWebSocket(
                self.settings,
                id="".join([el for el in self.get_guest_key_field() if el.isdigit()]),
                pwd=self.get_guest_password_field(),
                show_capture=self.show_capture,
            )
            self.guest_websocket.daemon = True
            self.guest_websocket.start()
            while self.guest_websocket.channel is None:
                if self.guest_websocket.error:
                    wx.MessageBox(str(self.guest_websocket.error), 'Error', wx.OK | wx.ICON_ERROR)
                    self.set_status_guest(str(self.guest_websocket.error))
                    self.guest_disconnect_action(None)
                    return None
                time.sleep(.2)

            self.guest_watcher = Thread(target=self.watch_process_guest)
            self.guest_watcher.start()
            self.set_guest_disconnect_button()
            self.resize()
        else:
            wx.CallAfter(wx.MessageBox, _("Please, type ID and password"), 'Info', wx.OK | wx.ICON_INFORMATION)

    def guest_disconnect_action(self, event):
        self.kill_guest_process()
        self.set_guest_connect_button()
        self.guest_empty_fields()
        self.resize()

    def resize(self, first_launch=None):
        self.panel.SetSizerAndFit(self.main_box)
        self.Fit()

    def set_host_key_field(self, value):
        self.host_support_id_field.SetValue(
            ' '.join([value[i:i + 3] for i in range(0, len(value), 3)])
        )

    def set_host_password_field(self, value):
        self.host_support_pwd_field.SetValue(value)

    def set_disable_host_connect_disconnect_buttons(self):
        self.host_support_connect_btn.Enable(False)
        self.host_support_disconnect_btn.Enable(False)

    def set_disable_guest_connect_disconnect_buttons(self):
        self.guest_support_connect_btn.Enable(False)
        self.guest_support_disconnect_btn.Enable(False)

    def set_host_connect_button(self):
        self.host_support_connect_btn.Enable(True)
        self.host_support_disconnect_btn.Enable(False)
        # wx.CallAfter(self.host_support_connect_btn.SetFocus)

    def set_guest_connect_button(self):
        self.guest_support_connect_btn.Enable(True)
        self.guest_support_disconnect_btn.Enable(False)

    def set_host_disconnect_button(self):
        self.host_support_connect_btn.Enable(False)
        self.host_support_disconnect_btn.Enable(True)
        # wx.CallAfter(self.host_support_disconnect_btn.SetFocus)

    def set_guest_disconnect_button(self):
        self.guest_support_connect_btn.Enable(False)
        self.guest_support_disconnect_btn.Enable(True)

    def host_empty_fields(self):
        self.host_support_id_field.SetValue('')
        self.host_support_pwd_field.SetValue('')

    def guest_empty_fields(self):
        self.guest_support_id_field.SetValue('')
        self.guest_support_pwd_field.SetValue('')
        # wx.CallAfter(self.guest_support_id_field.SetFocus)

    def set_status_host(self, message, arg=None):
        self.set_message(message)
        self.status_bar.SetStatusText(message, 0)

    def set_status_guest(self, message, arg=None):
        self.set_message(message)
        self.status_bar.SetStatusText(message, 1)

    def set_message(self, message):
        wx.CallAfter(log.info, message)

    def get_guest_key_field(self):
        return self.guest_support_id_field.GetValue()

    def get_guest_password_field(self):
        return self.guest_support_pwd_field.GetValue()

    def quit_action(self, e):
        e.Skip()

        self.kill_host_process()
        self.kill_guest_process()
        time.sleep(0.3)
        for item in wx.GetTopLevelWindows():
            item.Destroy()

    def kill_host_process(self):
        self.set_status_host('Disconnected')
        if self.host_websocket and self.host_websocket.running:
            self.host_websocket.stop()

    def kill_guest_process(self):
        self.set_status_guest('Disconnected')
        if self.guest_websocket and self.guest_websocket.running:
            self.guest_websocket.stop()

    def watch_process_host(self):
        status_set = None
        while self.host_websocket.running:
            if self.host_websocket.error:
                self.set_status_host(self.host_websocket.error)
                break
            if self.host_websocket.guest_ready is not None:
                if status_set is None:
                    self.set_status_host(_("Remote user is helping you..."))
                status_set = True
            time.sleep(.2)
        self.host_disconnect_action(None)

    def watch_process_guest(self):
        status_set = None
        while self.guest_websocket.running:
            if self.guest_websocket.error:
                self.set_status_guest(self.guest_websocket.error)
                break
            time.sleep(.2)
            if status_set is None:
                self.set_status_guest(_("Connected to the host"))
                status_set = True
        self.guest_disconnect_action(None)
