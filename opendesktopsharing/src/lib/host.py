#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
OpenDesktopSharing



The OpenDesktopSharing's goal is to access to remote computer across firewall and proxy:
- No needed to know IPs address
- Access only need KEY/PASSWORD
- Use websocket VPN to connect server and client
- Use Auth server (OpenDesktopSharing-Server) to generate auth access

@author: Pierre Arnaud <parnaud@easter-eggs.com>
@copyright: 2022

OpenDesktopSharing is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenDesktopSharing is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OpenDesktopSharing.  If not, see <http://www.gnu.org/licenses/>.
"""

import copy
import imp
import lz4.frame
import logging
from io import BytesIO
from threading import Lock
from PIL import Image
import mss
import numpy as np
from optparse import OptionParser
import os
import platform
import pickle
import time
import sys
import threading
import traceback
import websocket

from pynput.keyboard import Key,  Controller as KeyboardController
from pynput.mouse import Button, Controller as MouseController

from opendesktopsharing.src.lib.commons import logging_setup
from opendesktopsharing.src.lib.commons import humanbytes
from opendesktopsharing.src.lib.settings import Settings


log = logging.getLogger('OpenDesktopSharing')

mouse = MouseController()
keyboard = KeyboardController()


class HostWebSocket(threading.Thread):
    def __init__(self, settings, mode_cli=None):
        super(HostWebSocket, self).__init__()
        # websocket.enableTrace(True)

        self.settings = settings
        self.grabber = mss.mss()
        self.monitor = self.grabber.monitors[0]
        # Prevent strange resolution
        self.monitor['height'] = self.monitor['height'] if self.monitor['height'] % 2 == 0 else self.monitor['height'] - 1
        self.monitor['width'] = self.monitor['width'] if self.monitor['width'] % 2 == 0 else self.monitor['width'] -  1

        # Used to print session key in terminal in cli mode
        self.mode_cli = mode_cli

        self.ws = websocket.WebSocketApp(
            self.settings.client['auth'],
            header=[],
            on_message=self.on_message,
            on_error=self.on_error,
            on_close=self.on_close,
            on_open=self.on_open,
        )

        self.running = True
        self.forwarding = None
        self.guest_ready = None

        self.id = None
        self.pwd = None
        self.channel = None

        self.error = None

        self.thread_grab_image = threading.Thread(target=self.grab_image_loop, args=("Thread-1", ))
        self.thread_grab_image_running = False
        self.ready_to_send = None
        self.last_frame_sended = None
        self.lock = Lock()

    def on_message(self, *args):
        message = args[1] if len(args) == 2 else args[0]
        # log.debug('RECEIVING from websocket ({})'.format(humanbytes(len(message))))
        # DEBUG
        if not isinstance(message, bytes):
            return
        if b'SERVICE' in message:
            # COMMONS
            if b'SERVICE - CLOSE' in message:
                self.stop()
            elif b'SERVICE - FULL' in message:
                self.error = 'Server full'
                self.stop()
            elif b'SERVICE - MAX DURATION' in message:
                self.error = 'Max duration'
                self.stop()
            elif b'SERVICE - FORWARDING' in message:
                self.forwarding = True
            # HOST
            elif b'SERVICE - START HOST SESSION' in message:
                infos_connection = message.split(b' ')
                self.id = infos_connection[5].decode('utf-8')
                self.pwd = infos_connection[6].decode('utf-8')
                self.channel = infos_connection[7].decode('utf-8')
                if self.mode_cli:
                    # /!\ Do not remove this print
                    print('ID: {} Password: {}'.format(self.id, self.pwd))
            elif b'SERVICE - CONNECTED' in message:
                self.forwarding = True
                data_string = 'SERVICE - HOST SETTINGS ' + 'WIDTH=' + str(self.monitor['width']) + ' HEIGHT=' + str(self.monitor['height']) + ' SYSTEM=' + str(sys.platform)
                data_string = bytes(data_string, 'utf-8')
                self.send_data(data_string)
            elif b'SERVICE - NEED FRAME' in message:
                self.guest_ready = True
                self.send_frame()

        else:
            if b'MOUSE_EVT' in message:
                self.play_mouse(message)
            elif b'CHAR_EVT' in message:
                self.play_char(message)
            else:
                log.warning('Unknown message: '.format(message))

    def on_error(self, *args):
        log.info("### ERROR ###")
        log.error(traceback.print_exc())
        self.error = args[1] if len(args) == 2 else args[0]
        log.error(self.error)
        self.stop()

    def on_close(self, *args):
        log.info("### CLOSED ###")
        self.stop()

    def on_open(self, *args):
        to_send = b'SERVICE - HOST START'
        log.debug(to_send)
        self.send_data(to_send)

    def run(self):
        options = dict(
            sslopt=self.settings.sslopt,
            ping_interval=2,
        )
        if self.settings.client['use_proxy']:
            log.info('Use proxy')
            options.update(
                http_proxy_host=self.settings.client['proxy_host'],
                http_proxy_port=self.settings.client['proxy_port']
            )
        self.ws.run_forever(**options)

    def stop(self):
        log.info("### STOPPED ###")
        try:
            self.ws.send(b'SERVICE - CLOSE')
        except Exception as e:
            pass
        self.forwarding = None
        self.running = None
        self.channel = None
        self.guest_ready = None
        if self.thread_grab_image_running:
            self.thread_grab_image_running = False
            self.thread_grab_image.join()

    def send_data(self, data, opcode=websocket.ABNF.OPCODE_BINARY):
        # log.info('SENDING to websocket ({})'.format(humanbytes(len(data))))
        self.ws.send(data, opcode=opcode)

    def grab_image_loop(self, threadname):
        self.thread_grab_image_running = True
        first_loop = True
        while self.thread_grab_image_running:
            image = Image.frombytes(
                'RGB',
                (self.monitor['width'], self.monitor['height']),
                self.grabber.grab(self.monitor).rgb
            )
            with self.lock:
                if self.settings.client['colors'] != 'FULL':
                    # QUANTIZE
                    image = image.quantize(
                        colors=int(self.settings.client['colors']),
                        method=2,
                    )
                    image = image.convert('RGB')

                original = copy.deepcopy(image)
                if not first_loop and self.last_frame_sended:
                    diff = Image.fromarray(
                        np.array(image) - np.array(self.last_frame_sended)
                    )
                    image = diff

                payload = lz4.frame.compress(
                    image.tobytes(),
                    compression_level=9,
                    block_size=lz4.frame.BLOCKSIZE_MAX1MB,
                    return_bytearray=True,
                )

                self.ready_to_send = (payload, original, )
            time.sleep(.1)
            first_loop = False

    def send_frame(self):
        if self.thread_grab_image_running is False:
            self.thread_grab_image.start()

        while self.ready_to_send is None:
            time.sleep(.2)

        with self.lock:
            payload, self.last_frame_sended = self.ready_to_send
            self.ready_to_send = None

        log.debug('SEND {}'.format(humanbytes(len(payload))))
        self.ws.send(
            payload,
            opcode=websocket.ABNF.OPCODE_BINARY
        )

    def play_char(self, data):
        event_type, data = data.split(b' ', 1)
        data = pickle.loads(data)
        keycode = data.pop()
        key = data.pop()
        if all(x in data for x in ['ctrl', 'alt']):
            data.pop(data.index('ctrl'))
            data.pop(data.index('alt'))

        try:
            if key == '\x01':
                key = 'a'
            if key == '\x03':
                key = 'c'
            if key == '\x16':
                key = 'v'
            if key == '\x17':
                key = 'w'
            if key == '\x11':
                key = 'q'
            if key == '\x13':
                key = 's'
            if key == '\\t':
                key = Key.tab
            if len(data) >= 1 and data != ['shift']:
                hot_keys = data+[key]
                self.play_hotkeys(hot_keys)
            else:
                if keycode == 8:
                    keyboard.press(Key.backspace)
                    keyboard.release(Key.backspace)
                elif keycode == 9:
                    keyboard.press(Key.tab)
                    keyboard.release(Key.tab)
                elif keycode == 13:
                    keyboard.press(Key.enter)
                    keyboard.release(Key.enter)
                elif keycode == 27:
                    keyboard.press(Key.esc)
                    keyboard.release(Key.tab)
                elif keycode == 127:
                    keyboard.press(Key.delete)
                    keyboard.release(Key.delete)
                elif keycode == 310:
                    keyboard.press(Key.pause)
                    keyboard.release(Key.pause)
                elif keycode == 312:
                    keyboard.press(Key.end)
                    keyboard.release(Key.end)
                elif keycode == 313:
                    keyboard.press(Key.home)
                    keyboard.release(Key.home)
                elif keycode == 314:
                    keyboard.press(Key.left)
                    keyboard.release(Key.left)
                elif keycode == 315:
                    keyboard.press(Key.up)
                    keyboard.release(Key.up)
                elif keycode == 316:
                    keyboard.press(Key.right)
                    keyboard.release(Key.right)
                elif keycode == 317:
                    keyboard.press(Key.down)
                    keyboard.release(Key.down)
                elif keycode == 322:
                    keyboard.press(Key.insert)
                    keyboard.release(Key.insert)
                elif keycode == 340:
                    keyboard.press(Key.f1)
                    keyboard.release(Key.f1)
                elif keycode == 341:
                    keyboard.press(Key.f2)
                    keyboard.release(Key.f2)
                elif keycode == 342:
                    keyboard.press(Key.f3)
                    keyboard.release(Key.f3)
                elif keycode == 343:
                    keyboard.press(Key.f4)
                    keyboard.release(Key.f4)
                elif keycode == 344:
                    keyboard.press(Key.f5)
                    keyboard.release(Key.f5)
                elif keycode == 345:
                    keyboard.press(Key.f6)
                    keyboard.release(Key.f6)
                elif keycode == 346:
                    keyboard.press(Key.f7)
                    keyboard.release(Key.f7)
                elif keycode == 347:
                    keyboard.press(Key.f8)
                    keyboard.release(Key.f8)
                elif keycode == 348:
                    keyboard.press(Key.f9)
                    keyboard.release(Key.f9)
                elif keycode == 349:
                    keyboard.press(Key.f10)
                    keyboard.release(Key.f10)
                elif keycode == 350:
                    keyboard.press(Key.f11)
                    keyboard.release(Key.f11)
                elif keycode == 351:
                    keyboard.press(Key.f12)
                    keyboard.release(Key.f12)
                elif keycode == 366:
                    keyboard.press(Key.page_up)
                    keyboard.release(Key.page_up)
                elif keycode == 367:
                    keyboard.press(Key.page_down)
                    keyboard.release(Key.page_down)
                else:
                    keyboard.type(key)
        except Exception as e:
            log.error('except sending key: \n{}'.format(str(e)))

    def play_mouse(self, mouse_event):
        data_type, data = mouse_event.split(b' ', 1)
        data_type = data_type.decode('utf-8')
        data = pickle.loads(data)
        if data_type == 'MOUSE_EVT_MOTION':
            mouse.position = data

        elif data_type == 'MOUSE_EVT_MOUSE_EVENTS':
            mouse.position = data
            mouse.press(Button.left)
        elif data_type == 'MOUSE_EVT_LEFT_UP':
            mouse.position = data
            mouse.release(Button.left)

        elif data_type == 'MOUSE_EVT_MIDDLE_DOWN':
            mouse.position = data
            mouse.press(Button.middle)
        elif data_type == 'MOUSE_EVT_MIDDLE_UP':
            mouse.position = data
            mouse.release(Button.middle)

        elif data_type == 'MOUSE_EVT_RIGHT_DOWN':
            mouse.position = data
            mouse.press(Button.right)
        elif data_type == 'MOUSE_EVT_RIGHT_UP':
            mouse.position = data
            mouse.release(Button.right)

        elif data_type == 'MOUSE_EVT_LEFT_DCLICK':
            mouse.position = data
            mouse.click(Button.left, 2)

        elif data_type == 'MOUSE_EVT_MOUSEWHEEL':
            if data[0] > 0:
                mouse.scroll(0, 20)
            else:
                mouse.scroll(0, -20)

    def play_hotkeys(self, keys):
        for key in keys:
            if key == 'ctrl':
                key = Key.ctrl
            if key == 'shift':
                key = Key.shift
            if key == 'alt':
                key = Key.alt
            keyboard.press(key)
        for key in reversed(keys):
            if key == 'ctrl':
                key = Key.ctrl
            if key == 'shift':
                key = Key.shift
            if key == 'alt':
                key = Key.alt
            keyboard.release(key)
