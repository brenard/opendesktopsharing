#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
OpenDesktopSharing



The OpenDesktopSharing's goal is to access to remote computer across firewall and proxy:
- No needed to know IPs address
- Access only need KEY/PASSWORD
- Use websocket VPN to connect server and client
- Use Auth server (OpenDesktopSharing-Server) to generate auth access

@author: Pierre Arnaud <parnaud@easter-eggs.com>
@copyright: 2022

OpenDesktopSharing is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenDesktopSharing is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OpenDesktopSharing.  If not, see <http://www.gnu.org/licenses/>.
"""


import copy
import logging
import numpy as np
import pickle
from PIL import Image
import sys
import traceback
import wx
import wx.lib.inspection

from opendesktopsharing.src.lib import commons

log = logging.getLogger('OpenDesktopSharing')

try:
    _('test')
except Exception as e:
    def _(data):
        return data

def event_to_dict(event):
    event_dict = {
        'AltDown': event.AltDown(),
        'CmdDown': event.CmdDown(),
        'ControlDown': event.ControlDown(),
        'GetKeyCode': event.GetKeyCode(),
        'GetModifiers': event.GetModifiers(),
        'GetPosition': event.GetPosition(),
        # 'GetPositionTuple': event.GetPositionTuple(),
        'GetRawKeyCode': event.GetRawKeyCode(),
        'GetRawKeyFlags': event.GetRawKeyFlags(),
        # 'GetUniChar': event.GetUniChar(),
        'GetUnicodeKey': event.GetUnicodeKey(),
        'GetX': event.GetX(),
        'GetY': event.GetY(),
        'HasModifiers': event.HasModifiers(),
        'MetaDown': event.MetaDown(),
        'ShiftDown': event.ShiftDown(),
    }
    return event_dict

class ShowCapture(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, None, -1, "Capture",
            pos=wx.DefaultPosition,
            size=wx.DefaultSize,
            style=wx.DEFAULT_FRAME_STYLE,
        )

        self.socket_send_data = None
        self.socket_stop = None

        self.settings = None

        self.running = True

        self.width = None
        self.height = None

        self.count = 0
        self.last_image = None

        # wx.lib.inspection.InspectionTool().Show()


        self.SetTitle(_("OpenDesktopSharing: Loading..."))
        self.SetBackgroundColour((0, 0, 0))

        self.panel = wx.Panel(self)
        self.bmp = wx.StaticBitmap(self.panel)

        # BIND MOUSE AND KEYBORAD
        self.bmp.Bind(wx.EVT_MOUSE_EVENTS, self.mouse_action)

        # TODO: Check why win and linux have different behaviour
        self.panel.Bind(wx.EVT_CHAR, self.char_action)
        self.Bind(wx.EVT_CHAR, self.char_action)

        self.SetFocus()

        self.Bind(wx.EVT_CLOSE, self.exit)
        self.panel.Bind(wx.EVT_CLOSE, self.exit)

        self.eventDict = {}
        for name in dir(wx):
            if name.startswith('EVT_'):
                evt = getattr(wx, name)
                if isinstance(evt, wx.PyEventBinder):
                    self.eventDict[evt.typeId] = 'MOUSE_' + name

    def load_image(self, data):
        log.debug('Loading DATA... {}'.format(self.count))
        try:
            log.debug('Get frame and convert to RGB')
            image = np.frombuffer(data)
            image = Image.frombytes('RGB', (self.width, self.height), image)

            if self.last_image:
                patched = np.array(
                    self.last_image
                ) + np.array(
                    image
                )
                image = Image.fromarray(patched)

            self.last_image = copy.deepcopy(image)

            image = image.convert('RGB')
            log.debug('Resize image')
            image.thumbnail(self.panel.GetSize(), Image.ANTIALIAS)

            log.debug('Show image')
            to_bmp = wx.Bitmap.FromBuffer(
                image.size[0],
                image.size[1],
                image.tobytes()
            )
            self.bmp.SetBitmap(to_bmp)
            log.debug('Show image: DONE')
        except Exception as e:
            log.error('Show image: FAILED')
            log.error(traceback.format_exc())
            log.error(e)

        self.count += 1

    def exit(self, evt):
        self.running = False
        if self.socket_stop:
            self.socket_stop()
        self.Destroy()

    def char_action(self, event):
        keycode = event.GetKeyCode()
        if keycode == wx.WXK_F11 and event.ControlDown():
            if self.IsFullScreen():
                self.ShowFullScreen(False)
            else:
                self.ShowFullScreen(True)
        else:
            try:
                log.debug('CHAR: {}'.format(chr(event.GetRawKeyCode())))
                if self.bmp.IsEnabled() is True:
                    key_event = event_to_dict(event)
                    keys = []
                    if key_event['ControlDown'] is True:
                        keys.append('ctrl')

                    if key_event['AltDown'] is True:
                        keys.append('alt')

                    if key_event['ShiftDown'] is True:
                        keys.append('shift')

                    if 'darwin' in sys.platform:
                        keys.append(chr(key_event['GetKeyCode']))
                    else:
                        keys.append(chr(key_event['GetRawKeyCode']))

                    keys.append(key_event['GetKeyCode'])

                    data = b'CHAR_EVT ' + pickle.dumps(keys)
                    self.socket_send_data(data)
            except Exception as e:
                log.error(traceback.print_exc())
            event.Skip()

    def mouse_action(self, event):
        if self.bmp.IsEnabled() is True:
            factor = (
                float(self.width) / float(self.bmp.GetSize()[0]),
                float(self.height) / float(self.bmp.GetSize()[1])
            )
            mouse_position = (
                int(event.GetPosition()[0] * factor[0]),
                int(event.GetPosition()[1] * factor[1]),
            )

            action = {
                'MOUSE_EVT_MOUSE_EVENTS': mouse_position,
                'MOUSE_EVT_LEFT_UP': mouse_position,

                'MOUSE_EVT_MIDDLE_DOWN': mouse_position,
                'MOUSE_EVT_MIDDLE_UP': mouse_position,

                'MOUSE_EVT_RIGHT_DOWN': mouse_position,
                'MOUSE_EVT_RIGHT_UP': mouse_position,

                'MOUSE_EVT_LEFT_DCLICK': mouse_position,

                'MOUSE_EVT_MOUSEWHEEL': [event.GetWheelRotation()],
            }
            if mouse_position[0] % 25 == 0 or mouse_position[1] % 25 == 0 or event.Moving() is False:
                action.update({'MOUSE_EVT_MOTION': [mouse_position[0], mouse_position[1]]})
            if action.get(self.eventDict[event.GetEventType()]):
                data = self.eventDict[event.GetEventType()].encode('utf-8') + b' ' + pickle.dumps(
                    action[self.eventDict[event.GetEventType()]]
                )
                self.socket_send_data(data)
        event.Skip()
