#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
OpenDesktopSharing



The OpenDesktopSharing's goal is to access to remote computer across firewall and proxy:
- No needed to know IPs address
- Access only need KEY/PASSWORD
- Use websocket VPN to connect server and client
- Use Auth server (OpenDesktopSharing-Server) to generate auth access

@author: Pierre Arnaud <parnaud@easter-eggs.com>
@copyright: 2022

OpenDesktopSharing is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenDesktopSharing is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OpenDesktopSharing.  If not, see <http://www.gnu.org/licenses/>.
"""


import logging
import os
import pkg_resources
import pkgutil
import sys
import tempfile

import opendesktopsharing.share # Do not remove: Used for pyinstaller


log = logging.getLogger('OpenDesktopSharing')


def logging_setup(options):

    log = logging.getLogger('OpenDesktopSharing')
    if options.debug:
        log.setLevel(logging.DEBUG)
    else:
        log.setLevel(logging.INFO)

    # Console handler (root logger)
    handler = logging.StreamHandler(sys.stderr)
    if options.debug:
        handler.setLevel(logging.DEBUG)
    elif options.info:
        handler.setLevel(logging.INFO)
    else:
        handler.setLevel(logging.ERROR)

    formatter = logging.Formatter('%(asctime)s [%(name)s] %(levelname)-5.5s %(message)s')
    handler.setFormatter(formatter)

    log.addHandler(handler)


def get_locale_path():
    # Use debian path
    path = '/usr/share/opendesktopsharing/locale'
    if os.path.isdir(path):
        real_path = path
    else:
        # Use relative path
        path = '{}/../../../share/locale/'.format(
            os.path.dirname(os.path.realpath(__file__)),
        )
        path = os.path.abspath(path)
        if os.path.isdir(path):
            real_path = path
        else:
            # Use pkg path
            path = pkg_resources.resource_filename('opendesktopsharing.share', 'locale')
            real_path = path
    log.debug(real_path)
    return real_path


def get_data_file_path(module, data_file):
    real_path = None
    # Use debian path
    path = '/usr/share/opendesktopsharing/{}/{}'.format(module, data_file)
    if os.path.isfile(path):
        real_path = path
    else:
        # Use relative path
        path = '{}/../../../share/{}/{}'.format(
            os.path.dirname(os.path.realpath(__file__)),
            module,
            data_file,
        )
        path = os.path.abspath(path)
        if os.path.isfile(path):
            real_path = path
        else:
            # Use pkg path
            f = tempfile.NamedTemporaryFile(delete=False)
            data = pkgutil.get_data("opendesktopsharing.share", module + "/" + data_file)
            f.write(data)
            f.close()
            path = f.name
            if os.path.isfile(path):
                real_path = path
    log.debug(real_path)
    return real_path


def humanbytes(B):
    """
    Return the guestn bytes as a human friendly KB, MB, GB, or TB string
    From: https://stackoverflow.com/questions/12523586/python-format-size-application-converting-b-to-kb-mb-gb-tb
    """
    B = float(B)
    KB = float(1024)
    MB = float(KB ** 2) # 1,048,576
    GB = float(KB ** 3) # 1,073,741,824
    TB = float(KB ** 4) # 1,099,511,627,776

    if B < KB:
        return '{0} {1}'.format(B,'Bytes' if 0 == B > 1 else 'Byte')
    elif KB <= B < MB:
        return '{0:.2f} KB'.format(B/KB)
    elif MB <= B < GB:
        return '{0:.2f} MB'.format(B/MB)
    elif GB <= B < TB:
        return '{0:.2f} GB'.format(B/GB)
    elif TB <= B:
        return '{0:.2f} TB'.format(B/TB)

