# OpenDesktopSharing

The OpenDesktopSharing's goal is to access to remote computer across firewall and proxy:
- No needed to know IPs address
- Access only need KEY/PASSWORD
- Use websocket VPN to connect server and client
- Use Auth server to generate auth access

On GNU/Linux, Wayland is not supported as host

## Install

Use the prebuild executables from [Website](https://opendesktopsharing.easter-eggs.com/)

Or to install from source, see [Install guide](https://framagit.org/easter-eggs/opendesktopsharing/blob/master/INSTALL.md)


## Use

    # Server mode:
    $ opendesktopsharing --server

    # Client GUI mode
    $ opendesktopsharing

    # Client host only mode
    $ opendesktopsharing --host

    # Client guest only mode
    $ opendesktopsharing --guest 1234-5678

    # Help
    $ opendesktopsharing --help

In each mode you can use --info and --debug to set debug level

## Troubleshooting

- Keyboard not fully functional
- Scrolling on touchpad does not work
- GNU/Linux Wayland is not supported

## To report bug:

Got to [Framagit issue](https://framagit.org/easter-eggs/opendesktopsharing/issues) and please use this template to report issue:

For **incoming** and **outgoing** sides:
- OS and distribution/version (ex: Linux Debian Jessie, Linux Ubuntu 16.04, Macos Sierra, Windows 10 pro, ect ...)
- Plateform (32bit / 64bit)
- X server (GNU/Linux only) (X11 / Wayland)
- Window manager (GNU/Linux only)  (XFCE4 / GNOME / KDE / ect...)
- Screen resolution
- Executable type (script python (venv ?) / standalone version 32bit or 64bit / Windows installer / .app / .deb)
- Proxy ( yes / no)
- Antivirus (yes / no) (if yes, try to desactive it)
- Firewall (yes / no) (if yes, try to desactive it)


Trouble (Describe the trouble)
[...]


Log (Launch appli with args " --debug > log/txt")
[...]