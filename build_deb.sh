#!/bin/bash

set -e

# Enter source directory
cd $( dirname $0 )

# Clean previous build
rm -fr deb_dist

# Set version in pyproject.toml
if [ -z "$VERSION" ]
then
	VERSION=`git describe --tags|sed 's/^[^0-9]*//'`
	sed -i "s/^__version__ *=.*$/__version__ = '$VERSION'/" opendesktopsharing/__init__.py
fi

# Build debian source package
python3 setup.py --command-packages=stdeb.command sdist_dsc \
	--maintainer "Easter-eggs <info@easter-eggs.com>" \
	--section net

# Keep only debian package directory and orig.tar.gz archive
find deb_dist/ -maxdepth 1 -type f ! -name '*.orig.tar.gz' -delete

# Enter in debian package directory
cd deb_dist/opendesktopsharing-$VERSION

# Patch to install icon
cat << EOF >> debian/opendesktopsharing.install
opendesktopsharing.svg usr/share/icons/hicolor/scalable/apps/
EOF

# Generate debian changelog using generate_debian_changelog.py
python3 ../../generate_debian_changelog.py \
        --package-name opendesktopsharing \
        --version "${VERSION}" \
        --code-name $( lsb_release -c -s ) \
        --output debian/changelog \
        --path ../../ \
        --verbose

# Build debian package
dpkg-buildpackage
